<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Validator;
use Auth;
use Illuminate\Support\Facades\DB;

class AdminArticlesEditController extends Controller
{
    public function execute(Article $article, Request $request)
    {
            //ONLY FOR ADMIN!!
        $user = Auth::user();
        if ($user->name == 'admin') {

            if ($request->isMethod('DELETE')) {
            $article->delete();
            return redirect('admin')->with('status', 'Article was deleted');
            }
        } else {
            //сообщение о превышении Прав
            $mistake = 'You don`t have a right to DELETE Service!';
            // return redirect()->route('servicesAdd')->withErrors($mistake)->withInput();
            return redirect('admin')->withErrors($mistake);
        }

        if($request->isMethod('POST')){
            $input = $request->except('_token');
            $validator = Validator::make($input, [
                'title' => 'required|max:255',
                'text' => 'required',
                //'alias' => 'required|max:255|unique:pages,alias,'.$input['id']
                'alias' => 'required|max:255|unique:articles,alias->ignore'.$input['id']

            ]);
            if($validator->fails()){
                return redirect()
                    ->route('articlesEdit', ['article'=>$input['id']])
                    ->withErrors($validator);
            }
//            if($request->hasFile('images')) {
//                $file = $request->file('images');
//                $file->move(public_path().'/assets/img', $file->getClientOriginalName());
//                $input['images'] = 'img/'.$file->getClientOriginalName();
//            } else {
//                $input['images'] =  $input['old_images'];
//            }
//            unset($input['old_images']);


            $article->fill($input);

            if($article->update()) {
                return redirect('admin')->with('status', 'Article was update');
            }
        }


//        $art = DB::select("SELECT * from `articles` where id = ($article->id - 1)");
//        dump($art);
//        die();

        $old = $article->toArray();

        if(view()->exists('admin.articlesEditIndex')){
            $data = [
                'title' => 'Editing article "'.$old['title'].'"',
                'data' => $old
            ];
            return view('admin.articlesEditIndex', $data);
        }
    }
}
