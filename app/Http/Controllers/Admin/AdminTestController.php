<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminTestController extends Controller
{
    public function execute(){


//        return view('home');



        $user = Auth::user();
        $data = [
          'user' => $user
        ];

        return view('email.confirm_mail', $data);
    }
}
