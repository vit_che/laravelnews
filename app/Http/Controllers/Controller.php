<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Article;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailClass;
use Auth;
use App\User;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function ownerAdd ($user, $service, $name){

//        $article = Article::all();

        if ($user->verified == 1) {
//        if ($user->name == 'admin') {
//        if ($user->name == $article->author_name ) {

            if($service->save()) {
                return redirect('admin')->with('status', $name.' was Add');
            }
        }  else {

//            $mistake = 'You don`t have a right to add '.$name.'!';
            $error = 'You is not VERIFIED user '.$name.'!';
            return redirect()->route($name.'sAdd')->withErrors($error)->withInput();
            //  return route($name.'sAdd')->withErrors($mistake)->withInput();
        }
    }

    public function sendConfirm($email)
    {

        $confirm_code = hash('md5', $email);
        
        $email = 'dokerserv@gmail.com';
        
        Mail::to($email)->send(new MailClass($confirm_code));

        return $confirm_code;
    }
    
    public function confirmEmail($token)
    {
        $user = User::where('token', $token)
                ->first();

        if (isset($user)) {

            $user->verified = 1;

            if ($user->update()) {

                $status = 'You EMAIL IS VERIFIED SUCCESSFULLY!!';

                return redirect('home')->with('status', $status);

            } else {

                $error = 'Your Email verification data wasn`t saved! Try again.';

                return redirect('home')->withErrors( $error);
            }

        } else {

            $error = "You EMAIL IS NOT VERIFIED!!";

            return redirect('home')->withErrors( $error);
        }
    }
}
