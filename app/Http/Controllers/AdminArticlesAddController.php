<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Article;

class AdminArticlesAddController extends Controller
{
    public function execute(Request $request){

        if ($request->isMethod('post')) {
            $input = $request->except('_token');

            $messages = [
                'required' => 'Поле :attribute обязательно к заполнению',
                'unique' => 'Поле :attribute должно быть уникальным'
            ];

            $validator = Validator::make($input,[
                'title' => 'required|max:255',
                'alias' => 'required|unique:articles|max:50',
                'text' => 'required'
            ], $messages);

            if($validator->fails()) {
                return redirect()->route('articlesAdd')->withErrors($validator)->withInput();
            }

//            if($request->hasFile('images')){
//                $file = $request->file('images');
//                $input['images'] = 'img/'.$file->getClientOriginalName();
//                $file->move(public_path().'/assets/img', $input['images']);
//            }

            $article = new Article();

            $article->fill($input);

            $user = Auth::user();

//            $article->author_name = $user->name;


            Controller::ownerAdd($user, $article, 'article');
        }


        if(view()->exists('admin.articlesAddIndex')) {
            $data = [
                'title' => 'New Article'
            ];
            return view('admin.articlesAddIndex', $data);
        }
        abort(404);
    }
}
