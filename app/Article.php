<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['title','alias','text',
        'category_id', 'author_id', 'created_at', 'updated_at' ];
}
