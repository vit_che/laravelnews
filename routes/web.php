<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::group(['middleware' => 'web'], function () {
//    Route::auth();
//});

Route::group(['middleware'=>'web'], function () {

    Route::match(['get', 'post'], '/', ['uses' => 'IndexController@execute', 'as' => 'main']);
//    Route::get('/service/{alias}', ['uses' => 'ServiceController@execute', 'as' => 'service']); //для отображения Отдельной Страницы
//    Route::get('/page', ['uses' => 'PageController@execute', 'as' => 'page']); //для отображения Отдельной Страницы

    //    Route::get('/register/confirm/{token}', ['uses' =>'Auth\RegisterController@confirmEmail']);

    //    Route::get('/sendConfirm', ['uses' =>'Controller@sendConfirm', 'as' => 'sendConfirm']);
    
    Route::get('/confirmEmail/{token}', ['uses' =>'Controller@confirmEmail', 'as' => 'confirmEmail']);

//    Route::get('/test', ['uses' => 'Admin\AdminTestController@execute']);

    Route::get('/test', 'Admin\AdminTestController@execute');
    
//    Route::auth();  //для Админа
});


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    //Admin   for main admin page
    Route::get('/', function () {
        if (view()->exists('admin.index')) {
            $data = ['title' => 'Admin Panel'];
            return view('admin.index', $data);
        }
    })->name('adm');


    //admin/pages
    Route::group(['prefix' => 'articles'], function () {
        //admin/pages
        Route::get('/', ['uses' => 'AdminArticlesController@execute', 'as' => 'articles']);
        //admin/pages/add
        Route::match(['get', 'post'], '/add', ['uses' => 'AdminArticlesAddController@execute', 'as' => 'articlesAdd']);
        //admin/edit/2
        Route::match(['get', 'post', 'delete'], '/edit/{article}', ['uses' => 'AdminArticlesEditController@execute', 'as' => 'articlesEdit']);
    });


});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
