 <hr class="my-4">
 <br>
 <h3 class="text-center">{{ $title }}</h3>
 <h4></h4>
 <br>
 <hr class="my-4">

<div class="wrapper container-fluid">
        {!! Form::open(['url' => route('articlesEdit', array('articles'=>$data['id'])), 'class'=>'form-horizontal', 'method'=>'POST', 'enctype'=>'multipart/form-data' ])!!}

            <div class="form_group">
                <div class="row">
                    <div class="col-sm-1"></div>
                       {!! Form::hidden('id', $data['id']) !!}
                       {!! Form::label('title', 'Title:', ['class'=>'col-sm-1 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('title', $data['title'], ['class' => 'form-control', 'placeholder'=>'Input title!']) !!}
                    </div>
                </div>
            </div>
<br>
            <div class="form_group">
                <div class="row">
                    <div class="col-sm-1"></div>
                        {!! Form::label('alias', 'Alias:', ['class'=>'col-sm-1 control-label']) !!}
                    <div class="col-sm-8">
                       {!! Form::text('alias', $data['alias'], ['class' => 'form-control', 'placeholder'=>'Input alias page!']) !!}
                    </div>
                </div>
            </div>
<br>
            <div class="form_group">
                <div class="row">
                    <div class="col-sm-1"></div>
                        {!! Form::label('text', 'Text:', ['class'=>'col-sm-1 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::textarea('text', $data['text'], ['id'=>'editor','class' => 'form-control', 'placeholder'=>'Input text page!']) !!}
                    </div>
                </div>
            </div>
<br>

            {{--<div class="form_group">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-sm-1"></div>--}}
                        {{--{!! Form::label('old_images', 'Изображение:', ['class'=>'col-sm-1 control-label']) !!}--}}
                    {{--<div class="col-sm-offset-2 col-sm-8">--}}
                        {{--{!! Html::image('assets/'.$data['images'],'',['class'=>'img-circle img-responsive', 'width'=>'150px' ]) !!}--}}
                        {{--{!! Form::hidden('old_images', $data['images']) !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
{{--<br>--}}
            {{--<div class="form_group">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-sm-1"></div>--}}
                        {{--{!! Form::label('images', 'Изображение:', ['class'=>'col-sm-1 control-label']) !!}--}}
                    {{--<div class="col-sm-8">--}}
                    {{--{!! Form::file('images', ['class' => 'filestyle', 'data-buttonText'=>'Choose Image!', 'data-button'=>'button']) !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
{{--<br>--}}
            <div class="form_group">
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-offset-2 col-sm-8">
                        {!! Form::button('Save', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
                    </div>
                </div>
            </div>
<br>
         {!! Form::close() !!}

         <script>
         CKEDITOR.replace('editor');
         </script>

</div>



