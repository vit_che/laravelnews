<h2 class="text-center">ADD NEW ARTICLE</h2>

<div class="wrapper container-fluid">

{!! Form::open(['url'=>route('articlesAdd'),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}

     <div class="form-group">
        <div class="row">
        <div class="col-sm-1"></div>
            <div class="col-sm-1">
            {!! Form::label('name', 'Article Title', ['class' => 'control-label']) !!}
            </div>
            <div class="col-sm-8">
                {!! Form::text('title', old('title'), ['class' => 'form-control',
                                        'placeholder'=>'Введите название статьи']) !!}
            </div>
        </div>
     </div>

     <div class="form-group">
        <div class="row">
        <div class="col-sm-1"></div>
             {!! Form::label('alias', 'Alias', ['class' => 'col-sm-1 control-label']) !!}
             <div class="col-sm-8">
                 {!! Form::text('alias', old('alias'), ['class' => 'form-control',
                                         'placeholder'=>'Введите Alias']) !!}
             </div>
        </div>
     </div>


    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('category_id', 'Category', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                {!! Form::text('category_id', old('category_id'), ['class' => 'form-control',
                                        'placeholder'=>'Введите category_id']) !!}
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('author_id', 'Author', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                {!! Form::text('author_id', old('author_id'), ['class' => 'form-control',
                                        'placeholder'=>'Введите author_id']) !!}
            </div>
        </div>
    </div>

     <div class="form-group">
        <div class="row">
        <div class="col-sm-1"></div>
         {!! Form::label('text', 'Text', ['class' => 'col-sm-1 control-label']) !!}
         <div class="col-sm-8">
             {!! Form::textarea('text', old('text'), ['id'=>'editor','class' => 'form-control',
                                     'placeholder'=>'Введите TEXT']) !!}
         </div>
         </div>
     </div>

      {{--<div class="form-group">--}}
      {{--<div class="row">--}}
      {{--<div class="col-sm-1"></div>--}}
          {{--{!! Form::label('images', 'Изображение', ['class' => 'col-sm-1 control-label']) !!}--}}
          {{--<div class="col-sm-6">--}}
              {{--{!! Form::file('images',['class'=>'filestyle','data-buttonText'=>'Выберите файл']) !!}--}}
          {{--</div>--}}
      {{--</div>--}}
      {{--</div>--}}

     <div class="form-group">
     <div class="row">
     <div class="col-sm-2"></div>
         <div class="col-sm-offset-1 col-sm-8">
             {!! Form::button('Save', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
         </div>
     </div>
     </div>


{!! Form::close() !!}

            {{--<script>--}}
            {{--CKEDITOR.replace('editor');--}}
            {{--</script>--}}



</div>







<div class="container-fluid up-cont dark-gr" style="height: 20px"></div>
