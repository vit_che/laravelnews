<hr class="my-4">

<h2 class="text-center">ARTICLES CONTENT</h2>


<div class="container-fluid">

    @if($articles)

        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th># Number</th>
                <th>Title</th>
                <th>Alias</th>
                <th>Data Created</th>
                <th>Delete</th>
            </tr>
            </thead>

            @foreach( $articles as $article)
                <tr>
                    <td>{{$article->id}}</td>
                    <td>{!! Html::link(route('articlesEdit', ['article'=>$article->id]), $article->title,['alt'=>$article->title]) !!}</td>
                    <td>{{ $article->alias }}</td>
                    <td>{{ $article->created_at }}</td>
                    <td>
                        {!!  Form::open(['url' => route('articlesEdit', ['article'=>$article->id]), 'class'=>'form-horizontal', 'method'=>'POST'])    !!}
                        {{ method_field('DELETE') }}
                        {!! Form::button('Delete', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </table>

    @endif

    <button  type="button" class="bg-orange white btn btn-default bord "><span class="white">{!! Html::link(route('articlesAdd'), 'Create a New Article') !!}</span></button>




</div>

